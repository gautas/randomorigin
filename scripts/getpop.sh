#! /bin/bash
# grab census populations for states in 2019

curl "https://api.census.gov/data/2019/acs/acs5?get=NAME,group(B02001)&for=state:*" > ./../data/pop.json