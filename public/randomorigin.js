let store = {};

function loadData() {
  return Promise.all([
    d3.json(
      "https://api.census.gov/data/2019/acs/acs5?get=NAME,group(B02001)&for=state:*"
    ),
  ]).then((statesRaw) => {
    store.statesRaw = statesRaw[0];
    getStateObjs();
    addPopPcnts();
    addRacePopPcnts();
  });
}

function getStateObjs() {
  store.states = store.statesRaw
    .slice(1)
    .map((values) =>
      values
        .map((value, i) => ({ [store.statesRaw[0][i]]: value }))
        .reduce((o1, o2) => ({ ...o1, ...o2 }))
    );
}

function addPopPcnts() {
  store.states.map((state) => {
    state["total"] = parseInt(state.B02001_001E);
  });
  store.total = store.states
    .map((state) => state.total)
    .reduce((x, y) => x + y);
  store.states.map((state) => {
    state["pcnt"] = state.total / store.total;
  });
}

let raceVars = [
  { race: "white", var: "B02001_002E" },
  { race: "black", var: "B02001_003E" },
  { race: "american indian", var: "B02001_004E" },
  { race: "asian", var: "B02001_005E" },
  { race: "pacific islander", var: "B02001_006E" },
  { race: "other race", var: "B02001_007E" },
  { race: "multiracial", var: "B02001_008E" },
];

function addRacePopPcnts() {
  raceVars.map((raceVar) =>
    store.states.map((state) => {
      state[raceVar.race] = parseInt(state[raceVar.var]) / state.total;
    })
  );
}

function pickState() {
  let rand = Math.random();
  let currentState = 0;
  let totalPcnt = store.states[currentState]["pcnt"];
  while (rand > totalPcnt) {
    currentState += 1;
    totalPcnt += store.states[currentState]["pcnt"];
  }
  return store.states[currentState];
}

function pickRace(state) {
  let rand = Math.random();
  let currentRace = 0;
  let totalPcnt = state[raceVars[currentRace].race];
  while (rand > totalPcnt) {
    currentRace += 1;
    totalPcnt += state[raceVars[currentRace].race];
  }
  return raceVars[currentRace].race;
}

function pickOrigin() {
  let state = pickState();
  let race = pickRace(state);
  document.getElementById("origin").innerHTML =
    "You are born " + race + " in " + state["NAME"] + ".";
}

document.getElementById("refresh").addEventListener("click", pickOrigin);
loadData().then(pickOrigin);
